# -*- coding: utf-8 -*-

"""
Created on Tue Jul 27 08:33:48 2021

@author: yujiang
yujiangsenior@outlook.com
"""
### input: all the .dat files in various folders

import tensorflow as tf
import numpy as np
import os
import pandas as pd

###get the files' names in the directory
filedir = os.getcwd()#cwd:current working directory
allfiles = os.listdir(filedir)
dirnames = []#empty list

output_file = filedir + '/' + 'test.tfrecord'
for file in allfiles:#get a list of file names in the cwd
    if os.path.isdir(file):
        dirnames.append(file)

###get the filenames of .dat format under a certain directory
def get_filenames(filedir):
    allfiles = os.listdir(filedir)
    filenames = []
    for file in allfiles:#get a list of file names in the cwd
        if file.endswith('.dat'):
            filenames.append(file)
    return filenames

###dat_load: load content in .dat, and transfer to PX, PY, PZ
def dat_load(file):
    datContent = [i.strip().split() for i in open(file).readlines()]
    # with open("./2D_CFB_0.csv", "w") as f:
        # writer = csv.writer(f)
        # writer.writerows(datContent)
    # print(type(datContent))
    # RPM = pd.read_csv('./2D_CFB_0.csv', header=None, names=["PX","PY","PZ"])
    RPM = pd.DataFrame(datContent)#<class 'pandas.core.frame.DataFrame'>
    PX = RPM.iloc[:, 0]#<class 'pandas.core.series.Series'>
    PY = RPM.iloc[:, 1]
    PZ = RPM.iloc[:, 2]
    return PX, PY, PZ

###following is to concat T steps of position series into a DataFrame
def all_dat_in1case(filenames):
    k = 0
    for file in filenames:
        if k == 0:
            PX, PY, PZ = dat_load(file)# raw_data = dat_load('./2D_CFB_0.dat')
            PX_all = PX.to_frame()
            PY_all = PY.to_frame()
            PZ_all = PZ.to_frame()
        elif k>0:
            PX, PY, PZ = dat_load(file)
            PX_all = pd.concat([PX_all, PX], axis = 1)
            PY_all = pd.concat([PY_all, PY], axis = 1)
            PZ_all = pd.concat([PZ_all, PZ], axis = 1)
        k = k + 1
    PX_all.columns = np.arange(k)#change the columns index
    PY_all.columns = np.arange(k)
    PZ_all.columns = np.arange(k)
    return PX_all, PY_all, PZ_all

### Function that input DataFrame of positions data, output context and parsed_features
def get_context_features(PX, PY, PZ, key):
    shape = PX.shape ##(469,400)
    # print(type(PX))
    l1 = shape[0]    ##469
    l2 = shape[1]    ##400
    print(l1, l2)
    particle_type = np.array(5*np.ones(l1),dtype=np.int64)#[5,5,...,5,5] len=469
    context = {'particle_type':particle_type, 'key':key}
    
#    position = np.ndarray(shape=[l2,l1,3],dtype='float32')
    position = np.ndarray(shape=[l2,l1,2],dtype='float32')
    for i in range(l2): ##positions of 469 particles at timestep i
        for j in range(l1):            
#            position[i,j,:]=[PX.at[j,i],PY.at[j,i],PZ.at[j,i]]
            position[i,j,:]=[PX.at[j,i],PY.at[j,i]]
    parsed_features = {'position':position}
    return context, parsed_features

### start writing numpy ndarrays into tfrecord file.
def _bytes_featurelist(value):
    return tf.train.FeatureList(feature=[tf.train.Feature(bytes_list=tf.train.BytesList(value=[x_.tobytes()])) for x_ in value])
    
def _bytes_feature(value):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value.tobytes()]))
    
def _int64_feature(value):
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))


with tf.io.TFRecordWriter(output_file) as writer:
        # step1: construct all Feature
    key = 0
    for d in dirnames:#get a list of file names in the cwd
        os.chdir(d)#cd to 1 of the directory
        filenames = get_filenames(os.getcwd())#get the filenames with .dat format in the current directory
        PX_all, PY_all, PZ_all = all_dat_in1case(filenames)#combine all files together
#        context, parsed_features = get_context_features(PX_all, PY_all, PZ_all, key)#input positions, return 2 outcomes
###Following is 2D situation
        context, parsed_features = get_context_features(PX_all, PY_all, PY_all, key)#input positions, return 2 outcomes
        _CONTEXT_FEATURES = {
            'key': _int64_feature(context['key']),
            'particle_type':_bytes_feature(context['particle_type'])
        }
        feature_description = { # define Feature structure, tell decoder the type of each Feature
            'position': _bytes_featurelist(parsed_features['position']),
    #            'particle_type': _bytes_featurelist(context['particle_type'])
        }
        # step2: construct Features and FeatureLists
        features = tf.train.Features(feature=_CONTEXT_FEATURES)
        featurelists = tf.train.FeatureLists(feature_list=feature_description)
        # step3: construct an Example
        example = tf.train.SequenceExample(
                context=features,    ##key
                feature_lists=featurelists##position, particle_type
                ) # construct Example via dict
        # step4: Serialize 1 Example
        writer.write(example.SerializeToString())
        key = key + 1
        os.chdir(os.pardir)###cd back to the parent directory
