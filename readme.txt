### What is it used for?
This all_dat_to_tf.py is used to transform all the .dat files in any folders in the current working directory, to a .tfrecord file, used for Tensorflow framework.
### How to use
1. you need a conda environment containing tensorflow, numpy, pandas.
2.
$vim all_dat_to_tf.py
2.1 Then try to assign output_file (name of the output) in line 21
2.2 The default is for 2 dimensional data, if your dat is 3 dimensional, then uncomment line 46, 79, 83, 106, comment line 80, 84, 108
3.
$python all_dat_to_tf.py
